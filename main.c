#include "stm32f7xx_hal.h"
#include "gfx.h"

static GListener    gl;
static GHandle      ghFrame1;
static GHandle      ghSliderR, ghSliderG, ghSliderB;
static GHandle      ghButton1, ghButton2, ghButton3;
static GHandle      ghWindow1;

/**
 * HeartBeat thread which will keep flashing the LED
 * so we know that nothing crashed
 */
static threadreturn_t _heartbeat(void* arg)
{
	(void)arg;

	// LED    GPIOI1: output, pushpull, highspeed
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;					// Enable clock for PortA
	GPIOI->MODER |= GPIO_MODER_MODER1_0;					// Alternate function mode for PA5
	GPIOI->OTYPER &=~ GPIO_OTYPER_OT_1;						// Output Push/Pull mode
	GPIOI->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR1;				// PA5 output speed = HIGH SPEED

	while (TRUE) {
		HAL_GPIO_TogglePin(GPIOI, GPIO_PIN_1);
		gfxSleepMilliseconds(250);
	}

	return (threadreturn_t)0;
}

/**
 * Updates the color preview
 */
static void _updateColor(void)
{
	uint32_t color;

	color  = (unsigned)gwinSliderGetPosition(ghSliderR) << 16;
	color |= (unsigned)gwinSliderGetPosition(ghSliderG) <<  8;
	color |= (unsigned)gwinSliderGetPosition(ghSliderB) <<  0;

	gwinSetBgColor(ghWindow1, HTML2COLOR(color));
	gwinClear(ghWindow1);
}

/**
 * Create a demo GUI
 */
static void _guiCreate(void)
{
	GWidgetInit wi;

	// Apply some default values for GWIN
	gwinWidgetClearInit(&wi);
	wi.g.show = TRUE;

	// Create a surprise label behind the frame window
	wi.g.width = 100;
	wi.g.height = 20;
	wi.g.y = 100;
	wi.g.x = 150;
	wi.text = "Surprise!";
	gwinLabelCreate(0, &wi);

	// Apply the frame parameters
	wi.g.width = 440;
	wi.g.height = 231;
	wi.g.y = 20;
	wi.g.x = 20;
	wi.text = "ColorPicker";
	ghFrame1 = gwinFrameCreate(0, &wi, GWIN_FRAME_BORDER | GWIN_FRAME_CLOSE_BTN | GWIN_FRAME_MINMAX_BTN);

	// Apply the button parameters
	wi.g.width = 120;
	wi.g.height = 30;
	wi.g.x = 10;
	wi.g.y = 10;
	wi.text = "Random";
	wi.g.parent = ghFrame1;
	ghButton1 = gwinButtonCreate(0, &wi);

	// Apply the slider parameters
	wi.g.width = 290;
	wi.g.height = 30;
	wi.g.x = 140;
	wi.g.y += 0;
	wi.text = "Red";
	wi.g.parent = ghFrame1;
	ghSliderR = gwinSliderCreate(0, &wi);
	gwinSliderSetRange(ghSliderR, 0, 255);
	gwinSliderSetPosition(ghSliderR, 180);

	// Apply the button parameters
	wi.g.width = 120;
	wi.g.height = 30;
	wi.g.x = 10;
	wi.g.y += 40;
	wi.text = "Random";
	wi.g.parent = ghFrame1;
	ghButton2 = gwinButtonCreate(0, &wi);

	// Apply the slider parameters
	wi.g.width = 290;
	wi.g.height = 30;
	wi.g.x = 140;
	wi.g.y += 0;
	wi.text = "Green";
	wi.g.parent = ghFrame1;
	ghSliderG = gwinSliderCreate(0, &wi);
	gwinSliderSetRange(ghSliderG, 0, 255);
	gwinSliderSetPosition(ghSliderG, 60);

	// Apply the button parameters
	wi.g.width = 120;
	wi.g.height = 30;
	wi.g.x = 10;
	wi.g.y += 40;
	wi.text = "Random";
	wi.g.parent = ghFrame1;
	ghButton3 = gwinButtonCreate(0, &wi);

	// Apply the slider parameters
	wi.g.width = 290;
	wi.g.height = 30;
	wi.g.x = 140;
	wi.g.y += 0;
	wi.text = "Blue";
	wi.g.parent = ghFrame1;
	ghSliderB = gwinSliderCreate(0, &wi);
	gwinSliderSetRange(ghSliderB, 0, 255);
	gwinSliderSetPosition(ghSliderB, 235);

	// Color Preview
	wi.g.width = 420;
	wi.g.height = 70;
	wi.g.x = 10;
	wi.g.y += 40;
	ghWindow1 = gwinWindowCreate(0, &wi.g);

	_updateColor();
}

/**
 * A comment for this function? Really?
 */
int main(void)
{
	GEvent* pe;

	// Initialize the display
	gfxInit();

	// Set the widget defaults
	gwinSetDefaultFont(gdispOpenFont("*"));
	gwinSetDefaultStyle(&WhiteWidgetStyle, FALSE);
	gdispClear(White);

	// create the widget
	_guiCreate();

	// We want to listen for widget events
	geventListenerInit(&gl);
	gwinAttachListener(&gl);

	// Start the blinker (HeartBeat) thread
	gfxThreadCreate(0, 1024, NORMAL_PRIORITY, _heartbeat, 0);

	while(1) {
		// Get an Event
		pe = geventEventWait(&gl, TIME_INFINITE);

		switch(pe->type) {
			case GEVENT_GWIN_SLIDER:
				if (((GEventGWinSlider *)pe)->gwin == ghSliderR || \
													  ghSliderG || \
													  ghSliderB ) {
					_updateColor();
				}
				break;

			case GEVENT_GWIN_BUTTON:
				if (((GEventGWinButton *)pe)->gwin == ghButton1) {
					gwinSliderSetPosition(ghSliderR, rand() % 256);
				} else if (((GEventGWinButton *)pe)->gwin == ghButton2) {
					gwinSliderSetPosition(ghSliderG, rand() % 256);
				} else if (((GEventGWinButton *)pe)->gwin == ghButton3) {
					gwinSliderSetPosition(ghSliderB, rand() % 256);
				}

				_updateColor();

			default:
				break;
		}
	}
}

void exit(int status)
{
  (void)status;

  while (1) {
  }
}

void _init(void)
{
}
